This project is based on master thesis work by Mustaeen Ur Rehman Qazi titled "The machine learning lens:Using Generative Adversarial Networks to Detect Hidden Structure in Geological Images/Scenes
The branches of the repository are the different experiments perfomed to output high-resolution geological models.
Two SRGAN models(open source SRGAN models from GITHUB) are used which are slightly different with respect to architecture and configuration. Both SRGAN models are then modified according to the specific needs of Super-resolution in the geological context. Varoius data manipulation techniques are also applied to obtain best possible results.
First model is called TF_SRGAN and the second one is TL_SRGAN implemented in tensorflow and tensorlayer respectively. 
Links to the original SRGAN models are as follows:
https://github.com/HasnainRaz/Fast-SRGAN
https://github.com/tensorlayer/srgan
